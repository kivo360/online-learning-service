FROM tiangolo/uvicorn-gunicorn:python3.8

LABEL maintainer="Kevin Hill <tiangolo@gmail.com>"

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

RUN pip install --no-cache-dir fastapi

COPY ./machineserver /app