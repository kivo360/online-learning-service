from pypfopt.discrete_allocation import DiscreteAllocation
from pypfopt import EfficientFrontier, objective_functions
from pydantic import validate_arguments
from typing import Dict, TypeVar
import random, uuid

NumVar = TypeVar("NumVar", float, int)

absolute_values = {
    "process1": 0.60,
    "process2": 0.95,
    "process3": 0.25,
    "process4": 0.7,
}

weight_total = sum(absolute_values.values())

weights = {key: (value / weight_total) for key, value in absolute_values.items()}


@validate_arguments
def scale_values(all_values: Dict[str, NumVar]) -> Dict[str, NumVar]:
    """
    Scale the dictionary we receive from the allocator.

    >>> scale_values({"val_1": 100, "val_4": 100, "val_2": 100, "val_3": 100})
    {"val_1": 0.25, "val_4": 0.25, "val_2": 0.25, "val_3": 0.25}

    Parameters
    ----------
    all_values : Dict[str, NumVar]
        A dictionary representing the resources that are available.

    Returns
    -------
    Dict[str, NumVar]
        Scaled values that add up to 1.0
    """
    total = sum(all_values.values())
    return {key: (value / total) for key, value in all_values.items()}


def ran_name(dist: int = 5) -> str:
    return (uuid.uuid4().hex)[:dist]


@validate_arguments
def nom(mu: float, sigma: float) -> NumVar:
    return random.normalvariate(mu, sigma)


@validate_arguments
def gen_dynamic_task_values(num_tasks: int = 10) -> dict:
    return {f"option-{x}": nom(10, 3) for x in range(num_tasks)}


@validate_arguments
def get_resource_distribution(
    weight: Dict[str, NumVar],
    total_value: NumVar = 10_000,
) -> Dict[str, NumVar]:
    return {key: (value * total_value) for key, value in weight.items()}


# discrete_allocation = DiscreteAllocation(
#     weights, latest_prices, total_portfolio_value=20000
# )

# print(weights)
# print(get_resource_distribution({}, total_value=10000))

dynamic_values = gen_dynamic_task_values()
scaled = scale_values(dynamic_values)
print(get_resource_distribution(scaled))
