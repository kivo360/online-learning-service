from pyschedule import Scenario, solvers, plotters, alt
from pydantic import validate_arguments
from typing import Any, Literal, Optional, List, TypeVar
from uuid import uuid4

from machineserver.models.resources import ResourceAllocation, TaskSets
from loguru import logger
# the planning horizon has 10 periods
# A horizon unit equates to 100 assets observed.

ARBITRARY_DICT = dict(arbitrary_types_allowed=True)
HASHSIZE = 8


def ext_hash() -> str:
    return uuid4().hex[-HASHSIZE :]


class ClusterResourceManager(object):
    def __init__(
        self,
        experiment_name: str = "cluster_resouces",
        horizon: int = 10
    ) -> None:

        _mid_cpu_power = 3
        _mid_gpu_power = 10

        self.scene = Scenario(experiment_name, horizon=horizon)
        self.gpuRxMap = {
            "K80": ((_mid_gpu_power / 3.5) / 3.5),
            "P4": (_mid_gpu_power / 3.5),
            "T4": _mid_gpu_power,
            "P100": (_mid_gpu_power * 3.5),
            "V100": ((_mid_gpu_power * 3.5) * 3.5)
        }
        self.cpuRxMap = {
            "min": (_mid_cpu_power / 2),
            "middle": _mid_cpu_power,
            "max": (_mid_cpu_power * 2)
        }
        self.total_resources: Optional[ResourceAllocation] = None
        self.gpu_resource_list = []
        self.cpu_resource_list = []

        # These are all of the tasks
        self.tasks: List[Any] = []
        self.taskset: TaskSets = TaskSets.construct()

        self.cpu_total = 0
        self.gpu_total = 0

    @validate_arguments
    def add_gpus(
        self,
        gpu_name: Literal['K80', 'P4', 'T4', 'P100', 'V100'] = "T4",
        num_gpus: int = 1
    ):
        """
        add_gpus Add GPU

        Adds a GPU resource to the stochastic experiment.

        Parameters
        ----------
        gpu_name : Literal[, optional
            The name of the GPU type. It's limited in type., by default "T4"
        num_gpus : int, optional
            The number of GPUs we're using inside of the simulation, by default 1
        """
        gpu_size: int = int(self.gpuRxMap.get(gpu_name) * 0.7)
        gpu_resource = self.scene.Resource(
            f'{gpu_name}_GPU',
            size=gpu_size,
            num=num_gpus,
            gpu_cost=gpu_size,
        )
        self.gpu_total += num_gpus * gpu_size
        self.gpu_resource_list.append(gpu_resource)

    @validate_arguments
    def add_cpus(
        self,
        name: Literal["min", "middle", "max"] = "min",
        num_cpus: int = 1
    ):
        cpu_name = f"{name.capitalize()}_CPU"
        parallelization = int(self.cpuRxMap.get(name) * 0.7)
        cpu_resource = self.scene.Resource(
            cpu_name,
            size=parallelization,
            num=num_cpus,
            cpu_cost=parallelization
        )
        self.cpu_total += parallelization * num_cpus
        self.cpu_resource_list.append(cpu_resource)

    @validate_arguments(config=ARBITRARY_DICT)
    def set_available_compute(self, resource: ResourceAllocation) -> None:
        self.add_cpus(num_cpus=resource.cpus)
        self.add_gpus(num_gpus=resource.gpus)
        self.total_resources = resource

    @validate_arguments(config=ARBITRARY_DICT)
    def add_training(
        self, task_num: int = 1, gpu_cost: int = 5, cpu_cost: int = 2
    ):
        training_task_name = f"TrainingTask_{ext_hash()}"
        training_task = self.scene.Tasks(
            training_task_name,
            is_group=True,
            num=task_num,
            delay_cost=0.5,
            cpu_cost=cpu_cost,
            gpu_cost=gpu_cost
        )
        self.taskset.training.append(training_task)

    @validate_arguments(config=ARBITRARY_DICT)
    def add_inference(
        self, task_num: int = 10, gpu_cost: int = 2, cpu_cost: int = 5
    ):
        inference_task_name = f"InferenceTask_{ext_hash()}"
        inference_task = self.scene.Tasks(
            inference_task_name,
            is_group=True,
            num=task_num,
            delay_cost=4,
            cpu_cost=cpu_cost,
            gpu_cost=gpu_cost
        )
        self.taskset.inference.append(inference_task)

    @validate_arguments(config=ARBITRARY_DICT)
    def add_communication(
        self, task_num: int = 40, gpu_cost: int = 0, cpu_cost: int = 2
    ):
        combination_task_name = f"SimpleTask_{ext_hash()}"
        simple_task = self.scene.Tasks(
            combination_task_name,
            is_group=True,
            num=task_num,
            delay_cost=0.2,
            cpu_cost=cpu_cost,
            gpu_cost=gpu_cost
        )
        self.taskset.simple.append(simple_task)

    def allocate_resources_to_tasks(self):
        # Get all GPU resources
        gpus = [alt(x) for x in self.gpu_resource_list]
        cpus = [alt(x) for x in self.cpu_resource_list]
        gpu_cpu_tasks = self.taskset.inference + self.taskset.training
        cpu_only_tasks = self.taskset.simple

        # Set resource constraints
        for cpu_item in cpus:
            self.scene += cpu_item['cpu_cost'] < self.cpu_total

        for gpu_item in gpus:
            self.scene += gpu_item['gpu_cost'] < self.gpu_total

        full_set = gpus + cpus

        for cpu_task in gpu_cpu_tasks:
            cpu_task += full_set

        for cpu_task in cpu_only_tasks:
            cpu_task += cpus

    def solve(self):
        # Call the solution here.
        # Update the tasks

        self.allocate_resources_to_tasks()

        pass


if __name__ == "__main__":
    cluster_manager = ClusterResourceManager()
    cluster_manager.set_available_compute(
        ResourceAllocation(CPU=70, GPU=10, memory=0)
    )
    cluster_manager.add_training(task_num=12)
    cluster_manager.add_inference()
    cluster_manager.add_communication()
    cluster_manager.solve()
