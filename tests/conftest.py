# from _pytest import config
# import pytest
import socket, sys, time
from pydantic import (
    validate_arguments,
    BaseModel,
)
from loguru import logger
from xprocess import XProcess
from machineserver import TEST_PARAMS

SERVICE_NAME = "debug_service"


def getrootdir(config):
    return config.cache.makedir(".xprocess")


def local_xprocess(config):
    rootdir = getrootdir(config)
    return XProcess(config, rootdir)


@validate_arguments
def check_server(host: str = "localhost", port: int = 9900) -> bool:
    socket_checker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    location = (host, port)
    result_of_check = socket_checker.connect_ex(location)
    is_server = not bool(result_of_check)
    socket_checker.close()
    return is_server


def print_success():
    print("\n")
    logger.success(TEST_PARAMS.start_msg())


# def pytest_configure(config):
#     logger.warning(
#         "--------------------------------------------------------------"
#     )
#     logger.warning(
#         "------------------ Starting ML Service Test ------------------"
#     )
#     logger.warning(
#         "--------------------------------------------------------------"
#     )
#     local_xproc = local_xprocess(config)

#     if check_server(port=TEST_PARAMS.port, host=TEST_PARAMS.host):
#         # assert True, "Service should be up and running then."
#         print_success()
#         return

#     class Starter(ProcessStarter):
#         pattern = "Uvicorn running on "
#         args = ['poetry', 'run', 'debug_server']

#     local_xproc.ensure(SERVICE_NAME, Starter)

#     time.sleep(0.3)
#     assert check_server(
#         port=TEST_PARAMS.port, host=TEST_PARAMS.host
#     ), "The server hasn't been started"
#     print_success()

# def pytest_unconfigure(config):
#     logger.error("Service closed")
#     local_xproc = local_xprocess(config)
#     try:
#         local_xproc.getinfo(SERVICE_NAME).terminate()
#     except Exception as e:
#         logger.exception(e)
