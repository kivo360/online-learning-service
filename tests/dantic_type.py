import pytest
from pydantic import BaseModel
from machineserver.models.machine import NumpyDataType


class Converted(BaseModel):
    data: NumpyDataType


@pytest.mark.parameterize('transfered_data', [(10), (10.0), [10, 10.3, 10.5]])
def test_numpy_type(transfered_data):
    pass
