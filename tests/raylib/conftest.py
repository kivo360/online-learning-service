# from _pytest import config
# import pytest
import socket, sys, time
from pydantic import (
    validate_arguments,
    BaseModel,
)
from loguru import logger
from xprocess import ProcessStarter, XProcess
from machineserver import TEST_PARAMS

SERVICE_NAME = "debug_service"


def getrootdir(config):
    return config.cache.makedir(".xprocess")


def local_xprocess(config):
    rootdir = getrootdir(config)
    return XProcess(config, rootdir)


@validate_arguments
def check_server(host: str = "localhost", port: int = 9900) -> bool:
    socket_checker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    location = (host, port)
    result_of_check = socket_checker.connect_ex(location)
    is_server = not bool(result_of_check)
    socket_checker.close()
    return is_server


def print_success():
    print("\n")
    logger.success(TEST_PARAMS.start_msg())
