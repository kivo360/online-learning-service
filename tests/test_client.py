from typing import Union

from requests import Response
from machineserver.models import Response as MResponse
import numpy as np
from machineserver import defaults
from machineserver.client import MLClient


def success_check(response: Response, expected: Union[str, dict] = {}):
    # Extracting the core variables
    json_res = response.json()
    status_code = response.status_code
    _msgs = {
        "status_code": f"The request wasn't successful. Must have response code {defaults.SUCCESSFUL_RESPONSE_CODES}"
    }

    assert status_code in defaults.SUCCESSFUL_RESPONSE_CODES, _msgs["status_code"]
    assert json_res == expected


def test_send_prediction():
    print("\n")
    test_client = MLClient(port=8742)
    spec = np.linspace(0, 1000)
    prediction: MResponse = test_client.predict(spec)
    res_arr = prediction.body["item"]["data"]
    doubled = spec * 2
    assert np.allclose(doubled, res_arr), "The arrays aren't close."