# Machine Learning Server

This is a machine learning training and inference server that's to be used with any GPU based models in production.

The goal of this service is to properly deploy machine learning models of any kind, and to be able to scale them from a single machine to a cluster of 1000s of machines and 100s of GPUs. We scale the services up using the library `ray`, while we have the HTTP service built using `fastapi`.

The core use case, currently is to use it for causal inference. All commands happen via HTTP requests. The primary endpoints for this service are:

1. `/predict` - Here we push a tensor to make a prediction on from the client. This would be a set of features that we use to describe anything.
2. `/train` - This trains the model. We can decide to either push all of the training data to the service from the client, or have the service download the proper data by id to train.   
   1. Training will happen in the background using `ray`'s actors and a [parameter server](https://docs.ray.io/en/master/auto_examples/plot_parameter_server.html) configuration. This will allow models of various kinds to infinitely scale.
   2. In the future
3. `/status` - Returns the status of the actors and the server overall.


## Installation

This service uses poetry to operate. It's a python package manager.

First start by installing poetry.

```bash
brew install poetry
```

Then go to the root of the folder structure and install all of the packages. This will initiate a lengthy process of installing all libraries and their dependencies in order.

```bash
poetry install
```

## Running It

To run the server on dev mode (i.e, restart on save) run the command:

```bash
poetry run debug_server
```

To run tests dynamically, so they get run on save, use the command:

```bash
poetry run test_dev
```


# 10 Tests Run On This Service

1. Start a new detached ray actor on startup.
2. Turn the input NumPy array into a PyTorch tensor.
3. Run an operation on the PyTorch tensor.
4. Do a simple regression operation on the tensors you get (noise).
5. Initialize a Jamboree object at the beginning of a service. 
6. Send operation to the actor created on start.
7. Start sigma point classifier on start.
8. Start an online regressor on start.
9. Pull data on command.
   1.  Check that there's a certain amount of data pulled when a train command happens.
10. Pull synched items and train on them.