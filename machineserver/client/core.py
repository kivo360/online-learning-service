import inspect, threading
from machineserver.models import Response
from typing import Optional
from loguru import logger
import numpy as np
from machineserver.models import UvicornParameters, NumpyArray
from machineserver.utils import requests
from functools import wraps
from machineserver.utils.types import AnyDict
from machineserver.defaults import default_np_array


class LocalData(threading.local):
    def __init__(self):
        self.path: Optional[str] = None


THREAD_LOCAL_DATA = LocalData()


def api_endpoint(path: str = "/"):
    def inner_api_endpoint(f):
        @wraps(f)
        def wrapper(self, *args, **kw):
            self.set_path(path)
            return f(self, *args, **kw)

        return wrapper

    return inner_api_endpoint


class BaseClient(UvicornParameters):
    # path: Optional[str] = "/"

    protocol: str = "http"

    def path(self) -> str:
        return THREAD_LOCAL_DATA.path or "/"

    def set_path(self, update_path: str = "/"):
        THREAD_LOCAL_DATA.path = update_path

    @property
    def host_url(self) -> str:
        return f"{self.protocol}://{self.host}:{self.port}"

    @property
    def full_path(self) -> str:
        return f"{self.host_url}{self.path()}"

    # ---------------------------------------------------
    # ------------------- Commands ----------------------
    # ---------------------------------------------------

    def post(self, data: AnyDict) -> Response:
        response = requests.post(self.full_path, data)
        return Response(**response)

    def set_params(self, params: UvicornParameters):
        self.port: int = params.port
        self.host: str = params.host
        self.workers: int = params.workers
        self.location: str = params.location
        self.reload: bool = params.reload


# class MLClient(BaseClient):
#     @api_endpoint("/train")
#     def train(self, X=default_np_array(), y=default_np_array(), **kwargs):
#         self.post(dict(X=X, y=y))

#     @api_endpoint("/predict")
#     def predict(self, X):
#         return self.post(dict(arr=X))

#     @api_endpoint("/train_cmd")
#     def train_cmd(self):
#         pass

# if __name__ == "__main__":
#     sample_client = MLClient(port=8742)
#     logger.info(sample_client.predict())
