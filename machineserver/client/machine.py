from machineserver.models.http import Response
from loguru import logger
# from starlette import responses
from machineserver.client.core import BaseClient, api_endpoint
from machineserver.defaults import default_np_array

# class LocalData(threading.local):
#     def __init__(self):
#         self.path: Optional[str] = None

# THREAD_LOCAL_DATA = LocalData()


class MLClient(BaseClient):
    @api_endpoint("/train")
    def train(self, X_data=default_np_array(), y=default_np_array(), **kwargs):
        self.post(dict(X=X_data, y=y))

    @api_endpoint("/predict")
    def predict(self, X_data) -> Response:
        return self.post(dict(arr=X_data))

    @api_endpoint("/train_cmd")
    def train_cmd(self):
        pass