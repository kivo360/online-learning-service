import sys
import time

import ray
from fastapi import FastAPI
from loguru import logger

from machineserver import PING_MSG
from machineserver.models import DataMessage, NumpyData, ResourceAllocation, TaskSets
from machineserver.models.service import OptInput
from machineserver.utils import initalize_ray, kill_ray, start_ray_actors, grab_actor

version = f"{sys.version_info.major}.{sys.version_info.minor}"
import numpy as np

app = FastAPI()


@app.on_event("startup")
def startup_event():
    initalize_ray()
    start_ray_actors()
    logger.debug("Start ray actor if not exist.")


@app.on_event("shutdown")
def shutdown_event():
    with logger.catch():
        kill_ray()


@app.get("/")
async def read_root():
    return {"msg": PING_MSG}


@app.post("/predict", response_model=DataMessage)
async def predict(opt: OptInput):
    """
    For now just double the number you get into the parameters.
    """
    actor = grab_actor()
    if actor is not None:
        actor.increment.remote()
        logger.warning(ray.get(actor.get_counter.remote()))
    temporary_arr = np.array(opt.arr) * 2

    return DataMessage(
        item=NumpyData(data=temporary_arr), message="Successfully pulled prediction."
    )


@app.post("/train")
async def train():
    pass


@app.post("/status")
async def get_status():
    return {"message": "Should return the status here."}


@app.post("/train_cmd")
async def train_cmd():
    pass
