from typing import (
    Any,
    AnyStr,
    Dict,
    List,
    Union,
)

AnyDict = Dict[AnyStr, Any]
ListAny = List[Any]

AnyNum = Union[float, int]
NumList = List[AnyNum]
EitherListNum = Union[AnyNum, NumList]

__all__ = ['AnyDict', 'ListAny', 'AnyNum', 'NumList', 'EitherListNum']
