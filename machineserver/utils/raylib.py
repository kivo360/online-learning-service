from typing import Optional
import ray
from pebble import concurrent
from loguru import logger
from machineserver.actors.model_actor import ModelActor

MODEL_NAME: str = "model_actor"


def is_actor(name: str) -> bool:
    try:
        ray.get_actor(name)
        return True
    except ValueError as ve:
        return False


def initalize_ray():
    logger.info("Starting Ray")
    try:
        if not ray.is_initialized():
            ray.init(ignore_reinit_error=True)
    except Exception as e:
        logger.exception(e)
        logger.error("Ray failed to start.")
        return "Failure ... "
    logger.success("Successfully initialized ray into the system.")
    return "Success"



def kill_ray():
    if ray.is_initialized():
        ray.shutdown()
    logger.success("Successfully stopped ray.")


def start_ray_actors():
    if not is_actor(name=MODEL_NAME):
        Actor = ray.remote(ModelActor)
        Actor.options(name=MODEL_NAME, lifetime="detached").remote()
        logger.success("Model actor was successfully created.")
        return
    logger.error("The actor was not successfully started.")


def grab_actor() -> Optional[ModelActor]:
    if is_actor(name=MODEL_NAME):
        return ray.get_actor(MODEL_NAME)
    return None


__all__ = ["initalize_ray", "kill_ray", "start_ray_actors", "grab_actor"]