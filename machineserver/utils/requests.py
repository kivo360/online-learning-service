from typing import Any, AnyStr, Dict, List, Union

import httpx as requests
from loguru import logger
import numpy as np
from machineserver.utils import serialize

from machineserver.utils.types import AnyDict, ListAny
from machineserver.defaults import JSON_POST_HEADER

FAILED_RESOPNSE = {}


def post(url: str, json_data: Union[AnyDict, ListAny, np.ndarray] = {}) -> dict:
    data: AnyStr = serialize(json_data)
    response = FAILED_RESOPNSE
    try:
        response = requests.post(url, data, http_headers=JSON_POST_HEADER)
    except Exception as e:
        logger.exception(e)

    return response


__all__ = ["post"]
