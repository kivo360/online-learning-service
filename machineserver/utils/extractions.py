# from datetime import datetime
import re
from typing import Type
import numpy as np


def extract_nums(num_str: str):
    temp = re.findall(r'\d+', num_str)
    return list(map(int, temp))


def get_single_num(num_str: str):
    return extract_nums(num_str)[0]


def extract_type_from_list(value, T: Type):
    is_type = isinstance(value, T)
    is_list = isinstance(value, list)
    if not is_type and not is_list:
        raise TypeError(f"{value} must be type {T} or a list with that type.")
    if isinstance(value, list):
        if len(value) > 0:
            return T(value[0])
        else:
            raise AttributeError("You entered an empty list.")
    else:
        return value


def convert_numpy(item):
    k, v = item
    if not isinstance(v, dict):
        return k, v

    data_type = v.get('data_type', None)
    if not data_type:
        return k, v

    data = v.get("data", None)
    if not data:
        return k, v
    if not isinstance(data, (int, float, list)):
        return k, v
    v['data'] = np.array(data)
    return k, v