# from machineserver.utils import deserialize
from typing import Any, AnyStr, Dict, List, Union

import numpy as np
import orjson
from pydantic import validate_arguments

JSON_POST_HEADER = [("Content-Type", "application/json")]

AnyDict = Dict[AnyStr, Any]
ListAny = List[Any]


def serialize(data: dict):
    return orjson.dumps(
        data,
        option=orjson.OPT_SERIALIZE_NUMPY,
    )


@validate_arguments
def deserialize(input_str: AnyStr) -> Union[dict, list, int, float, str]:
    """Milk

    Parameters
    ----------
    input_str : str
        [description]

    Returns
    -------
    Union[dict, list, int, float, str]
        [description]
    """
    return orjson.loads(input_str)


__all__ = ["serialize", "deserialize"]
