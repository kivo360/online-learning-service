from enum import Enum
from pydantic import validate_arguments
from typing import Any, Dict, TypeVar, List
from machineserver.models.resources import MachineUnit, ComputeTask
from toolz import curry
from loguru import logger
from contextlib import suppress

NumVar = TypeVar("NumVar", float, int)
NON_DIVISIBLE = ["CPU"]


@validate_arguments
def scale_values(all_values: Dict[str, NumVar]) -> Dict[str, NumVar]:
    """
    Scale the dictionary we receive from the allocator.

    >>> scale_values({"val_1": 100, "val_4": 100, "val_2": 100, "val_3": 100})
    {"val_1": 0.25, "val_4": 0.25, "val_2": 0.25, "val_3": 0.25}

    Parameters
    ----------
    all_values : Dict[str, NumVar]
        A dictionary representing the resources that are available.

    Returns
    -------
    Dict[str, NumVar]
        Scaled values that add up to 1.0.
    """
    total = sum(all_values.values())
    return {key: (value / total) for key, value in all_values.items()}


@curry
def pget(current_dict: Dict, key: str, default: Any):
    return current_dict.get(key, default)


def calculate_processor_types(machines: List[MachineUnit]):
    if len(machines) <= 0:
        return {}
    mech_unit_calculations = {}
    cget = pget(mech_unit_calculations)
    for machine in machines:
        mech_unit_calculations[machine.name] = cget(machine.name, {})
        pget_name = pget(mech_unit_calculations[machine.name])

        mech_unit_calculations[
            machine.name]["CPU"] = (pget_name("CPU", 0.0) + machine.cpu)
        mech_unit_calculations[
            machine.name]["GPU"] = (pget_name("GPU", 0.0) + machine.cpu)
        mech_unit_calculations[machine.name
                               ]["CNT"] = pget_name("CNT", 0.0) + 1.0
    return mech_unit_calculations


def calculate_task_totals(tasks: List[ComputeTask]):
    task_totals = {"CPU": {}, "GPU": {}, "DELAY": {}, "TOTAL": {}}
    if len(tasks) <= 0:
        return task_totals
    for machine in tasks:
        task_totals["GPU"][machine.name] = machine.gpu_cost()
        task_totals["CPU"][machine.name] = machine.cpu_cost()
        task_totals["DELAY"][machine.name] = machine.delay_penalty
        task_totals["TOTAL"][machine.name] = machine.total_cost()
    return task_totals


def calculate_resource_weights(
    aggregated_compute: Dict[str, Dict[str, float]]
):

    # Figure out who needs the most compute
    resource_weights = {}
    for key, val in aggregated_compute.items():
        resource_weights[key] = 0
        with suppress(ZeroDivisionError):
            resource_weights[key] = scale_values(val)
    return resource_weights


@validate_arguments
def get_resource_distribution(
    weight: Dict[str, NumVar],
    total_value: NumVar = 10_000,
) -> Dict[str, NumVar]:
    return {key: (value * total_value) for key, value in weight.items()}


def resource_opt(
    res_wgt: Dict[str, Dict[str, float]], total_resources: MachineUnit
):
    td = total_resources.dict()
    td.pop("name", None)
    allo = {}
    for k, v in td.items():
        up_key = k.upper()
        curr_res_wgt = res_wgt[up_key]
        allo[up_key] = get_resource_distribution(curr_res_wgt, total_value=v)
        # logger.warning((up_key, v, RESOURCES))
    return allo


@curry
def cunit(gpu: float = 1, cpu: float = 1, name: str = "inference"):
    return MachineUnit(cpu=cpu, gpu=gpu, name=name)


def ctask(name: str):
    return ComputeTask(name=name)


class Elements(Enum):
    inf = "inference"
    train = "training"
    param = "parameter"


unit_sets = [
    cunit(name=Elements.inf.value),
    cunit(name=Elements.inf.value),
    cunit(name=Elements.train.value),
    cunit(name=Elements.train.value),
]

unit_totals = list(map(lambda x: ctask(x.value), Elements))
utilization_totals = calculate_task_totals(unit_totals)
res_weights = calculate_resource_weights(utilization_totals)
final_allocation = resource_opt(res_weights, cunit(gpu=5, cpu=100))
logger.error(final_allocation)
# logger.success(utilization_weights)
# logger.debug(calculate_task_totals(unit_totals))
# logger.info(calculate_processor_types(unit_sets))