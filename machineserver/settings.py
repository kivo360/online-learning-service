from pydantic import BaseSettings


class MLServerSettings(BaseSettings):
    primary_model: int
