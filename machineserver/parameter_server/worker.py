from pyschedule import Scenario, solvers, plotters, alt
from pydantic import validate_arguments, ValidationError, BaseModel
from typing import Literal
from uuid import uuid4

from machineserver.models.resources import ResourceAllocation
# the planning horizon has 10 periods
# A horizon unit equates to 100 assets observed.

# two resources: Alice and Bob
ARBITRARY_DICT = dict(arbitrary_types_allowed=True)

# inference_task = scene.Tasks("prediction", num=10)


class ClusterResourceManager(object):
    def __init__(
        self,
        experiment_name: str = "cluster_resouces",
        horizon: int = 10
    ) -> None:

        _mid_cpu_power = 4
        _mid_gpu_power = 10

        self.scene = Scenario(experiment_name, horizon=horizon)
        self.gpuRxMap = {
            "K80": ((_mid_gpu_power / 3.5) / 3.5),
            "P4": (_mid_gpu_power / 3.5),
            "T4": _mid_gpu_power,
            "P100": (_mid_gpu_power * 3.5),
            "V100": ((_mid_gpu_power * 3.5) * 3.5)
        }

        self.gpu_resource_list = []
        self.cpu_resource_list = []

        # These are all of the tasks
        self.tasks = dict(training=[], inference=[], simple=[])

    @validate_arguments
    def add_gpus(
        self,
        gpu_name: Literal['K80', 'P4', 'T4', 'P100', 'V100'] = "T4",
        num_gpus: int = 1
    ):
        """
        add_gpus Add GPU

        Adds a GPU resource to the stochastic experiment.

        Parameters
        ----------
        gpu_name : Literal[, optional
            The name of the GPU type. It's limited in type., by default "T4"
        num_gpus : int, optional
            The number of GPUs we're using inside of the simulation, by default 1
        """
        gpu_resource = self.scene.Resource(
            f'{gpu_name}_GPU', size=self.gpuRxMap.get(gpu_name), num=num_gpus
        )
        self.gpu_resource_list.append(gpu_resource)

    @validate_arguments
    def add_cpus(
        self,
        name: Literal["min", "middle", "max"] = "min",
        num_cpus: int = 1
    ):
        cpu_name = f"{name}_CPU"
        parallelization = self.cpuRxMap.get(name)
        cpu_resource = self.scene.Resource(
            cpu_name, size=parallelization, num=num_cpus
        )
        self.cpu_resource_list.append(cpu_resource)

    @validate_arguments(config=ARBITRARY_DICT)
    def set_available_compute(self, resource: ResourceAllocation) -> str:
        self.add_cpus(num_cpus=resource.cpus)
        self.add_gpus(num_gpus=resource.gpus)
        return str(resource)

    def add_training(self, task_num=1, gpu_cost=5, cpu_cost=2):
        training_task_name = f"TrainingTask-{uuid4().hex}"
        training_task = self.scene.Tasks(
            training_task_name,
            is_group=True,
            num=task_num,
            delay_cost=1.5,
            cpu_cost=cpu_cost,
            gpu_cost=gpu_cost
        )
        # self.tasks.append(training_task)

    def solve(self):
        # Call the solution here.
        pass


if __name__ == "__main__":
    cluster_manager = ClusterResourceManager()
    print(
        cluster_manager.set_available_compute(
            ResourceAllocation(CPU=20, GPU=0, memory=0)
        )
    )
from pyschedule import Scenario, solvers, plotters
S = Scenario('capacities_myattribute', horizon=10)
R = S.Resource('R')

# define the additional property named myproperty uniformly as 1
T = S.Tasks('T', num=4, length=1, delay_cost=1, myattribute=1)
# set it to 0 for the first task
T[0].myattribute = 0
T += R

# the sum of myproperty must be smaller than 3 until period 5
S += R['myattribute'][0 : 5] <= 3

solvers.mip.solve(S, msg=1)
print(S.solution())

# three tasks: cook, wash, and clean
cook = S.Task('cook', length=1, delay_cost=1)
wash = S.Task('wash', length=2, delay_cost=1)
clean = S.Task('clean', length=3, delay_cost=2)

# every task can be done either by Alice or Bob
cook += Alice | Bob
wash += Alice | Bob
clean += Alice | Bob

# compute and print a schedule
solvers.mip.solve(S, msg=1)
print(S.solution())
