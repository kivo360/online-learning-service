# import os
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from torchvision import datasets, transforms
# from filelock import FileLock
# import numpy as np

# import ray

# @ray.remote
# class ParameterServer(object):
#     def __init__(self, lr):
#         self.model = ConvNet()
#         self.optimizer = torch.optim.SGD(self.model.parameters(), lr=lr)

#     def apply_gradients(self, *gradients):
#         summed_gradients = [
#             np.stack(gradient_zip).sum(axis=0)
#             for gradient_zip in zip(*gradients)
#         ]
#         self.optimizer.zero_grad()
#         self.model.set_gradients(summed_gradients)
#         self.optimizer.step()
#         return self.model.get_weights()

#     def get_weights(self):
#         return self.model.get_weights()
