# import os
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from torchvision import datasets, transforms
# from filelock import FileLock
# import numpy as np

# import ray

# print("Running synchronous parameter server training.")
# current_weights = ps.get_weights.remote()
# for i in range(iterations):
#     gradients = [
#         worker.compute_gradients.remote(current_weights) for worker in workers
#     ]
#     # Calculate update after all gradients are available.
#     current_weights = ps.apply_gradients.remote(*gradients)

#     if i % 10 == 0:
#         # Evaluate the current model.
#         model.set_weights(ray.get(current_weights))
#         accuracy = evaluate(model, test_loader)
#         print("Iter {}: \taccuracy is {:.1f}".format(i, accuracy))

# print("Final accuracy is {:.1f}.".format(accuracy))
# # Clean up Ray resources and processes before the next example.
# ray.shutdown()
