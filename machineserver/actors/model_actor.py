from typing import List
import ray
from pydantic import BaseModel, Field, PositiveInt


# @ray.remote
class ModelActor:
    def __init__(self):
        self.value = 0

    def increment(self):
        self.value += 1
        return self.value

    def get_counter(self):
        return self.value


class MachineUnit(BaseModel):
    cpu: PositiveInt
    gpu: int


class Task(BaseModel):
    name: str
    gpu_pace_per_sec: float = Field(
        ...,
        description="The movement of the task (processing) within the second done by the GPU.",
    )
    cpu_pace_per_sec: float = Field(
        ...,
        description="The movement of the task (processing) within the second done by the CPU.",
    )
    gpu_mass: float = Field(
        ...,
        description="The size of the information that needs to be processed by the GPU.",
    )
    cpu_mass: float = Field(
        ...,
        description="The size of the information that needs to be processed by the CPU.",
    )


class ResourceAllocation(BaseModel):
    cpus: PositiveInt = Field(..., alias="CPU")
    gpus: float = Field(0, alias="GPU", ge=0)
    memory: float = Field(0, ge=0)
    tasks: List[Task] = []

    class Config:
        extra = "ignore"

    def __str__(self) -> str:
        return self.__repr__()


# TODO: Finish the basic allocator math at some point.
if __name__ == "__main__":
    ray.init()

    resources = ResourceAllocation(**{**ray.cluster_resources(), **{"GPU": 0}})
    print(resources.gpus)
    print(resources.cpus)
