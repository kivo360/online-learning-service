import numpy as np
JSON_POST_HEADER = [('Content-Type', 'application/json')]
SUCCESSFUL_RESPONSE_CODES = [200, 201, 202, 203, 204, 205]


def default_np_array() -> np.ndarray:
    return np.linspace(0, 1000, num=100)