from pydantic import BaseConfig
from .http_calls import JSON_POST_HEADER, SUCCESSFUL_RESPONSE_CODES, default_np_array
val_config = dict(arbitrary_types_allowed=True)


class ValConfig(BaseConfig):
    arbitrary_types_allowed: bool = True
