from auto_all import start_all, end_all
from pydantic import BaseConfig, Extra

start_all(globals())


class Arbitrary(BaseConfig):
    arbitrary_types_allowed = True
    validate_all = True


class ArbitraryIgnore(Arbitrary):
    extra = Extra.ignore


class ArbitraryAllow(Arbitrary):
    extra = Extra.allow


class ArbitraryForbid(Arbitrary):
    extra = Extra.forbid


end_all(globals())
