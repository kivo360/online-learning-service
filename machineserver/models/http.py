from datetime import datetime
import re
from typing import Any, Dict, Type, Union
from numpy.core.numeric import flatnonzero
from toolz import itemmap
import numpy as np
from pydantic import BaseModel, validator, Field
from machineserver.defaults import SUCCESSFUL_RESPONSE_CODES
from machineserver.utils import deserialize
from machineserver.models.machine import NumpyData, DataItem


def extract_nums(num_str: str):
    temp = re.findall(r'\d+', num_str)
    return list(map(int, temp))


def get_single_num(num_str: str):
    return extract_nums(num_str)[0]


def extract_type_from_list(value, T: Type):
    is_type = isinstance(value, T)
    is_list = isinstance(value, list)
    if not is_type and not is_list:
        raise TypeError(f"{value} must be type {T} or a list with that type.")
    if isinstance(value, list):
        if len(value) > 0:
            return T(value[0])
        else:
            raise AttributeError("You entered an empty list.")
    else:
        return value


def convert_numpy(item):
    k, v = item
    if not isinstance(v, dict):
        return k, v
    # logger.info(v)
    data_type = v.get('data_type', None)
    if not data_type:
        return k, v

    data = v.get("data", None)
    if not data:
        return k, v
    if data_type == "__numpy__":
        v['data'] = np.array(data)
    v.pop("data_type", None)
    return k, v


class DataScienceBody(BaseModel):
    """Data Science Body

    Parameters
    ----------
    BaseModel : [type]
        [description]
    """
    pass


class ConfigIgnore(BaseModel):
    class Config:
        extra = "ignore"


class Headers(ConfigIgnore):

    server: str = None
    content_length: int = Field(alias="content-length")

    @validator('server', pre=True, always=True)
    def extract_server_name(cls, v):
        return extract_type_from_list(v, str)


class DataMessage(ConfigIgnore):
    message: str = "This is the response to the user."
    item: DataItem = DataItem()


class Response(ConfigIgnore):
    version: float
    status: int = None
    content_length: int = Field(alias="content-length")
    content_type: str = Field(alias="content-type")
    body: Dict[str, Any]

    @property
    def is_success(self):
        return self.status in SUCCESSFUL_RESPONSE_CODES

    @validator('status', pre=True, always=True)
    def extract_number_str(cls, v):
        return get_single_num(v)

    @validator('content_length', pre=True, always=True)
    def extract_content_length(cls, v):
        return extract_type_from_list(v, str)

    @validator('body', pre=True, always=True)
    def extract_date(cls, v):
        if isinstance(v, str) or isinstance(v, bytes):
            return deserialize(v)
        elif isinstance(v, dict):
            return v
        raise TypeError("Body should be broken.")

    @validator('body', pre=False, always=True)
    def convert_ml(cls, v):
        # logger.info(v)

        if not v:
            return v
        return itemmap(convert_numpy, v)
