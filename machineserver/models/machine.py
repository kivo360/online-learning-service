from pydantic import BaseModel, validator

from typing import Any, Optional, Union

import numpy as np
from loguru import logger
from machineserver.utils.types import *


class DataItem(BaseModel):
    data_type: str = "__generic__"
    data: Optional[Any] = None


class NumpyData(DataItem):
    data_type: str = "__numpy__"
    data: Optional[EitherListNum] = None

    @validator('data', pre=True, always=True)
    def convert_ndarray(cls, v):
        if isinstance(v, np.ndarray):
            return v.tolist()
        return v

    @validator('data_type', always=True)
    def is_numpy_str(cls, v):
        if v != "__numpy__":
            raise ValueError("The data_type isn't __numpy__")
        return v

    def __str__(self) -> str:
        return self.__repr__()


class NumpyDataFloatType(float):
    pass
