import ray
import random
from typing import Dict, List
from pydantic import BaseModel, Field, PositiveInt, confloat
from pyschedule import Task as SchedTask
from machineserver.models import ArbitraryIgnore
from pypfopt import objective_functions


class ComputeTask(BaseModel):
    name: str
    # How much we loose if we delay processing these tasks.
    # Very useful when considering inference tasks
    delay_penalty: confloat(ge=0, le=0.5) = 0.0
    io_cost: confloat(ge=0, le=1) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 1)
    )
    gpu_mass: confloat(ge=0, le=5) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 5)
    )
    gpu_vel: confloat(ge=0, le=5) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 5)
    )
    cpu_mass: confloat(ge=0, le=5) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 5)
    )
    cpu_vel: confloat(ge=0, le=5) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 5)
    )
    # The amount the given calculation can be parallel.
    # Between 0 and 1.
    # 0 represents it can't have many workers
    # 1 represents it can absolutely have multiple workers
    parallel: confloat(ge=0, le=1) = Field(
        description="Default is only an example of the output.",
        default_factory=lambda: random.uniform(0, 1)
    )

    class Config:
        extra = "allow"

    def total_cost(self) -> float:
        return self.cpu_cost() + self.gpu_cost()

    def cpu_cost(self) -> float:
        return self.cpu_mass * self.cpu_vel

    def gpu_cost(self) -> float:
        return self.gpu_mass * self.gpu_vel


class MachineUnit(BaseModel):
    name: str
    cpu: confloat(ge=0)
    gpu: confloat(ge=0)


class TaskSets(BaseModel):
    # __config__ = ArbitraryIgnore

    training: List[SchedTask] = []
    inference: List[SchedTask] = []
    simple: List[SchedTask] = []

    class Config(ArbitraryIgnore):
        pass


class ResourceAllocation(BaseModel):
    __config__ = ArbitraryIgnore

    cpus: PositiveInt = Field(..., alias="CPU")
    gpus: float = Field(0, alias="GPU", ge=0)
    memory: float = Field(0, ge=0)

    class Config(ArbitraryIgnore):
        pass

    def __str__(self) -> str:
        return self.__repr__()


#
