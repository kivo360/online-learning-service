# from loguru import logger
from typing import Any, List, TypeVar, Union

import numpy as np
from loguru import logger
from pydantic import BaseModel

AnyNum = TypeVar("AnyNum", int, float)
NumList = List[AnyNum]
EitherListNum = Union[AnyNum, NumList]


class UvicornParameters(BaseModel):
    port: int = 8000
    host: str = "0.0.0.0"
    workers: int = 4
    location: str = 'machineserver.main:app'
    reload: bool = True

    class Config:
        arbitrary_types_allowed = True


class UvicornTestParameters(UvicornParameters):
    port: int = 8742
    host: str = "0.0.0.0"
    workers: int = 2
    location: str = 'machineserver.main:app'
    reload: bool = True

    def sub_options(self) -> List[str]:
        d: dict = self.dict()
        d.pop("location", None)

        parameter_list = []
        for key, value in d.items():

            if isinstance(value, bool):
                value = "true" if value else "false"
            parameter_list.append(f"--{key}")
            parameter_list.append(f"{value}")

        return parameter_list

    def uvicorn_subprocess_options(self) -> List[str]:
        options = self.sub_options()
        options.insert(0, self.location)
        options.insert(0, "uvicorn")
        return options

    def uvicorn_params(self):
        parameters: dict = self.dict()
        parameters.pop("location", None)
        return parameters

    def get_url(self, route: str = "/"):
        return f"http://{self.host}:{self.port}{route}"

    def start_msg(self) -> str:
        reload_msg = "It IS reloading" if self.reload else "It ISN'T reloading."
        return f"The service has started on {self.host}:{self.port}. {reload_msg}"


class OptInput(BaseModel):
    arr: List[Any]

    def as_numpy(self):
        opt_dict = self.dict()
        opt_dict['arr'] = np.array(self.arr)
        return opt_dict


class NumpyArray(np.ndarray):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        logger.info(field_schema)

    @classmethod
    def validate(cls, v: Any) -> np.ndarray:
        # if not isinstance(v, np.ndarray):
        #     raise TypeError("Not a numpy array")
        return v

    def __repr__(self):
        return f'NumpyArray({super().__repr__()})'


if __name__ == "__main__":
    logger.info(NumpyArray())
