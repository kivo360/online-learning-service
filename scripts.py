import sys
import subprocess
from machineserver import TEST_PARAMS
import uvicorn


def live_test():
    subprocess.run(['ptw', '--runner', "pytest --testmon"])


def start_debug_server():
    params = TEST_PARAMS.uvicorn_params()
    uvicorn.run(TEST_PARAMS.location, **params)
